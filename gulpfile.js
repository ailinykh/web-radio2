'use strict';

var path = require('path');
var gulp = require('gulp');
var browserSync = require('browser-sync').create();
var gutil = require('gulp-util');
var $ = require('gulp-load-plugins')({
  pattern: ['gulp-*', 'main-bower-files', 'gulp.*', 'uglify-save-license']
});

var conf = {
  paths: {
    src: 'src',
    dist: 'dist',
    tmp: '.tmp'
  }
};

var errorHandler = function(title) {
  'use strict';
  return function(err) {
    gutil.log(gutil.colors.red('[' + title + ']'), err.toString());
    this.emit('end');
  };
}

// Styles
gulp.task('styles-reload', function() {
  return buildStyles()
    .pipe(browserSync.stream());
});

gulp.task('styles', function() {
  return buildStyles();
});

var buildStyles = function() {
  return gulp.src(path.join(conf.paths.src, '/**/*.{scss,css}'))
    // .pipe($.debug())
    .pipe($.sass().on('error', errorHandler('sass')))
    .pipe(gulp.dest(path.join(conf.paths.tmp, '/serve/')))
    .pipe($.size({ title: path.join(conf.paths.tmp, '/serve/'), showFiles: true }));
}

// Scripts
gulp.task('scripts-reload', function() {
  return buildScripts()
    .pipe(browserSync.stream());
});

gulp.task('scripts', function() {
  return buildScripts();
});

var buildScripts = function() {
  return gulp.src([
      'node_modules/bootstrap-sass/assets/javascripts/*.min.js',
      'node_modules/jquery/dist/jquery.min.js',
      'node_modules/jquery-lazyload/jquery.lazyload.js',
      path.join(conf.paths.src, '/js/*.js')
    ])
    .pipe(gulp.dest(path.join(conf.paths.tmp, '/serve/js/')))
    .pipe($.size({ title: path.join(conf.paths.tmp, '/serve/'), showFiles: true }));
}

// Inject
gulp.task('inject-reload', ['inject'], function() {
  browserSync.reload();
});

gulp.task('html', function() {
  return gulp.src(path.join(conf.paths.src, '/*.pug'))
  .pipe($.pug({pretty: true}))
  .pipe(gulp.dest(path.join(conf.paths.tmp, '/serve')))
  .on('end', function() {
    gulp.start('inject')
  });
});

gulp.task('inject', function() {
  var jsFilter = $.filter('**/*.js', { restore: true });
  var styles = gulp.src(path.join(conf.paths.tmp, '/**/*.css'), { read: false });
  var scripts = gulp.src([
    path.join(conf.paths.tmp, '/**/jquery.min.js'), //include first!
    path.join(conf.paths.tmp, '/**/*.js')
  ], { read: false });
  var injectOptions = {
    ignorePath: [conf.paths.src, path.join(conf.paths.tmp, '/serve/')],
    // ignorePath: [path.join(conf.paths.tmp, '/serve/')],
    addRootSlash: false
  }

  return gulp.src(path.join(conf.paths.tmp, '/serve/*.html'))
    .pipe($.inject(styles, injectOptions))
    .pipe($.inject(scripts, injectOptions))
    .pipe(jsFilter)
    .pipe($.uglify({ preserveComments: $.uglifySaveLicense }))
    .pipe(jsFilter.restore)
    .pipe($.versionNumber({value: '%DT%'}))
    // .pipe($.useref({ searchPath: './' }))
    .pipe(gulp.dest(path.join(conf.paths.tmp, '/serve')))
    .pipe($.size({ title: path.join(conf.paths.tmp, '/serve'), showFiles: true }))
});

gulp.task('other', function () {
  var fileFilter = $.filter(function (file) {
    return file.stat.isFile();
  });

  return gulp.src([
    path.join(conf.paths.src, '/**/*'),
    path.join('!' + conf.paths.src, '/**/*.{scss,pug,css,scss,js}')
  ])
    .pipe(fileFilter)
    .pipe(gulp.dest(path.join(conf.paths.tmp, '/serve/')));
});

gulp.task('build', ['scripts', 'styles', 'html', 'other'], function () {
  gulp.src(path.join(conf.paths.tmp, '/serve/**/*.*'))
  .pipe(gulp.dest(conf.paths.dist))
   .on('end', function() {
     gulp.src(path.join(conf.paths.dist, '**/*.html'))
     .pipe($.useref())
     .pipe(gulp.dest(conf.paths.dist))
   });
});

gulp.task('serve', ['scripts', 'styles', 'html', 'other'], function(){
  var routes = {
    '/bower_components': 'bower_components'
  };
  browserSync.init({
    server: {
      baseDir: path.join(conf.paths.tmp, '/serve'),
      routes: routes
    },
    open: 'external'
  });

  gulp.watch(path.join(conf.paths.src, '**/*.scss'), ['styles-reload']);
  gulp.watch(path.join(conf.paths.src, '**/*.js'), ['scripts-reload']);
  gulp.watch(path.join(conf.paths.src, '**/*.pug'), ['html']);
  gulp.watch(path.join(conf.paths.tmp, '/serve/*.html'), function(event) {
    browserSync.reload(event.path);
  });
});
