$('.toggle-btn').click(function(e) {
  	e.preventDefault();

    var container = $(this).next().next().find('.slidable');
    var $this = $(this);

    if (container.hasClass('show')) {
        $this.text('Показать все');
        container.slideUp(350, function() {
          container.removeClass('show');
        });
    } else {
        $this.text('Скрыть все');
        container.slideToggle(350, function() {
          container.toggleClass('show');
          $("img.lazy").lazyload();
        });
    }
});

$(document).ready(function(){
  // lazy loading images
  $("img.lazy").lazyload();
  // popovers
  $('a.ppvr')
    .popover()
  $('a.volume.ppvr').on('shown.bs.popover', function() {
    $("#player-volume").change(function(e) {
      $("#player").jPlayer("volume", $(e.target).val());
    });
  });
});
